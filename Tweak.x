%hook TabbedLibraryViewController
-(BOOL)shouldShowTabForContentMode:(int)mode {
  return mode && (%orig);
}
%end
